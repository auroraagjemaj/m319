![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# UML Activity Diagramm

---

### Lernziel:
* Was ist UML?
* Was ist AD?
* Bedeutung der Symbole

--- 

## Einführung

Die **Unified Modeling Language** (vereinheitlichte Modellierungssprache), kurz **UML**, ist eine standardisierte, grafische Modellierungssprache zur Spezifikation, Konstruktion, Dokumentation und Visualisierung von Software-Teilen und anderen Systemen. Sie besteht aus verschiedenen Diagrammtypen: *Aktivitätsdiagramm, Anwendungsfallsdiagramm, Klassendiagramm, Sequenzdiagramm* und weitere ...

![UML](./x_gitressourcen/UML.png) [Wikipedia](https://de.wikipedia.org/wiki/Unified_Modeling_Language#Aktivit%C3%A4ten)

Ein **Aktivitätsdiagramm** (AD) ist ein Modell für einen Ablauf. Es besteht aus Aktionen, zwischen denen Kontroll- und Datenflüsse existieren. Ein AD stellt das **dynamische Verhalten** eines Software-Systems (Programms) dar. 
AD werden für *Prozesse*, *Handlungen*, *Algorithmen* und auch *Anwendungsfälle* verwendet. Sie können in Anforderungslisten die Funktion eines Programms darstellen.


### Die wichtigsten, standardisierten Symbole

| Symbole | Bedeutung | Anwendung |
|:---:|---|---|
|![Start Stop](./x_gitressourcen/StartStop.png)|Start- und Endknoten|Zeigt den Beginn und das Ende des Ablaufs|
|![Aktivität](./x_gitressourcen/Activity.png)|Aktivität <br> Aktion|Zeigt eine Aktivität und/oder Aktion an. Diese Symbole enthalten kurze Beschreibungen, die direkt in die jeweilige Form eingefügt werden, und bilden die Hauptbausteine eines Aktivitätsdiagramms. (Nomen & Verb) <br> *Hinweis: Es darf nur ein Fluss darauf zeigen!*|
|![Fluss](./x_gitressourcen/Flow.png)|Kontrollfluss|Pfeile zeigen den Kontrollfluss und führen von einem Element zum nächsten.|
|![Verzeigung](./x_gitressourcen/Merge.png)|Entscheidung, Verzweigung, Zusammenführung|Zeigt den Kontrollfluss bei einer Entscheidung oder Rückführung einer Schleife. Zusammenführungen des Kontrollflusses müssen zwingend mit einer Raute erfolgen. Es wird nicht auf die anderen Inputs gewartet.|
|![Call](./x_gitressourcen/Call Activity.png)|Aktivitäts-Aufruf|Zeigt, dass eine separate Aktivität aufgerufen wird. Somit kann eine Aktivität eine andere aufrufen. |
|![Fork](./x_gitressourcen/Fork.png)|Teilung, paralleler Kontrollfluss|Teilung initiiert parallele Prozesse, die gleichzeitig ablaufen|
|![Join](./x_gitressourcen/Join.png)|Synchronisation|Synchronisation zeigt die Zusammenführung. Der nächste Schritt ist nur möglich, wenn beide Prozesse abgeschlossen sind. |
|![Data](./x_gitressourcen/Data.png)|Daten Elemente|Bezeichnet beim Datenfluss bearbeitete Daten. Rechteck kann auch an der Aktivität angebunden sein. (Siehe komplexe ADs &#8594; A3)|

*Gelesen wird das Diagramm indem ein "Spielstein" (Token) beim Startknoten platziert wird und entlang dem Kontrollfluss von Aktion zu Aktion bis zu einem Endknoten weitergezogen wird. (Eine Teilung erzeugt weitere Token, eine Synchronisation verschluckt diese ausser einem wieder.)*

Hier ist ein Beispiel eines ADs: *Das Lernen eines Themas im SOL-Setting.*

![AD Beispiel](./x_gitressourcen/AD_Bsp.png) ![AD Beispiel](./x_gitressourcen/W3S_ausprobieren.png)


### Erklärung:

**Startknoten und Endknoten verwenden**<br>
Wichtig ist, dass jeder Ablauf einen Start und ein Ende hat.

**Zusammenführung** <br>
Zusammenführende Kontrollflüsse *müssen* mit einer Raute gekennzeichnet werden.

**Aktivität** <br>
Rechtecke mit runden Ecken! Es soll darauf geachtet werden, dass nur ein Kontrollfluss hinein geht. Ausgänge können mehrere sein (Entscheidungen erfolgen dann innerhalb der Aktivität).

**Teilungen** <br>
Teilungen (mit dem schwarzen Balken) zeigen auf, dass gleichzeitig zwei Abläufe geschehen (im obigen Beispiel: Lernen und Praxis gleichzeitig mit Notieren). Wichtig ist, dass eine Teilung stets am Schluss wieder zusammen kommt:

**Synchronisation** <br>
Wenn parallele Prozesse wieder zusammengeführt werden, dann darf es nur einen Ausgang (d.h. einen nächsten Schritt) geben. Erst wenn alle KOntrollflüsse eintreffen geht der Ablauf weiter.

**Verzeigung** <br>
"Rauten" ermöglichen, dass Kontrollflüsse bedingt verzweigen können. Die Bedingung wird beim Kontrollfluss-Pfeil angegeben (nicht *in* der Raute): `[Bedingung]`.

**Aktivitäts-Aufruf** <br>
"Aktivität als Aufruf" ermöglichen, ein weiteres, separat definiertes AD auszuführen, d.h. aufzurufen. Der Aufruf-Name sollte im separaten AD als Überschrift ersichtlich sein (oft auch im Dateinamen). Der Kontrollfluss springt an Schluss automatisch wieder zurück zum Aufruf.

Es gibt noch weitere Elemente in einem AD. So kann man z.B. auch Benachrichtigungen darstellen (Signale senden und empfangen). Man kann auch einen zeitlichen Trigger darstellen (wenn z.Bsp. eine Aktion zu einem bestimmten Zeitpunkt ausgelöst wird). Es kann ein Datenfluss im Kontrollfluss integriert werden. (Siehe [N3-More_AD](../N3-More_AD) &#8594; A3)

Hier gibt’s mehr Details, u.a. auch zur Darstellung von verschachtelten Abläufen:

<https://sourcemaking.com/uml/modeling-business-systems/external-view/activity-diagrams>

<https://de.wikipedia.org/wiki/Aktivit%C3%A4tsdiagramm>

### Cheat Sheet AD:
[UML V2.5 - AD Übersicht](./Notationsuebersicht UML 2.5 3.pdf)


---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## Wahl eines UML-Tools:

Werkzeuge, um Aktivitätsdiagramme darzustellen: <br>
*Achtung: Symbolik muss korrekt sein! Bei einigen Tools fehlen gewisse Symbole!*
		
**DrawIO** (Empfohlenes Tool UML >> UML 2.5 öffnen): <https://www.draw.io/>
*Hinweis: Verwenden Sie die Vorlage "AD Symbols.drawio"*

**StarUML** (Freies ProfiTool WIN/mac): <http://staruml.io/>

**Modelio** (Freies Profitool WIN) : <https://www.modelio.org/>

**UMLet** (UMLetino online): <https://www.umlet.com/>

**Violet** (Erzeugt Html-Diagramme, braucht Java 8): <http://alexdp.free.fr/violetumleditor/page.php>

**Visual Paradigm Online Editor** (Freies Profitool): <https://online.visual-paradigm.com/de/diagrams/solutions/free-activity-diagram-editor-online/>

Oder natürlich auch **MS Visio** ([>>> TBZ EDU](https://portal.azure.com/#blade/Microsoft_Azure_Education/EducationMenuBlade/software)) oder andere Graphikprogramme.

---

## Einführung AD mit DrawIO

![Video:](../x_gitressourcen/Video.png)
[![Einführung](./x_gitressourcen/DrawIO.png)](https://web.microsoftstream.com/video/e3caa725-e52e-4702-b374-92b93af82778?list=studio)

[Tutorials Drawio](https://drawio-app.com/tutorials/)
---

## Einführung AD mit StarUML

![Video:](../x_gitressourcen/Video.png)
[![Einführung StarUML](./x_gitressourcen/StarUML.png)](https://web.microsoftstream.com/video/61f4cc4b-f420-4d6b-b072-88bfab229a43)

[Doku StarUML AD](http://staruml.sourceforge.net/docs/user-guide(en)/ch05_6.html)

---

# Checkpoint
* Kenne die Symbole von AD (Standard UML 2.5+!)
* Kann ein einfaches AD lesen und verstehen.
* Habe ein UML-Tool installiert und kann ADs erstellen ...


<br><br><br>

# English version

## Learning goals
* What is UML?
* What is AD?
* Symbols used for AD

--- 

## Introduction

**Unified Modeling Language** (or **UML**) is standardized graphical modelling language, which is used to specify, document and visualize software components. UML consists of several types of diagrams, the most common ones being *Activity Diagram, Use Case Diagram, Class Diagram, Sequence Diagram*.

![UML](./x_gitressourcen/UML.png) [Wikipedia](https://en.wikipedia.org/wiki/Activity_diagram)

The **Activity Diagram** (AD) is a graphical representation of a workflow. It consists of actions, and shows choice, iteration and concurrency. ADs show the **dynamic behaviour** of a software program. Although activity diagrams primarily show the overall flow of control, they can also include elements showing the flow of data between activities through one or more data stores.
ADs are used for *processes*, *activities*, *algorithms* and also *use cases*.


### The most important standardized symbols

| Symbol | Meaning | Usage |
|:---:|---|---|
|![Start Stop](./x_gitressourcen/StartStop.png)|Start and end point|Shows the beginning and end of a workflow|
|![Activity](./x_gitressourcen/Activity.png)|Activity <br> Action|Represents an activity or action. These symbols include a short description, which can be added directly in the symbol. They are the main elements of the diagram. (Nomen & Verb) <br> *Note: Only one flow is allowed to point to the activity*|
|![Flow](./x_gitressourcen/Flow.png)|Control flow|Arrows show the control flow and lead to the next element.|
|![Merge](./x_gitressourcen/Merge.png)|Decision, Branching, Merging|Shows the control flow for a decision or return for a loop. If flows are merged back, they must be shown with the diamond symbol. The flow does not wait for other inputs.|
|![Call](./x_gitressourcen/Call Activity.png)|Activity Call|It shows, that a separate activity is being called. This allows to call another separate activity.|
|![Fork](./x_gitressourcen/Fork.png)|Forking, parallel control flow|Shows parallel processes, which occur simultaneously.|
|![Join](./x_gitressourcen/Join.png)|Join|Shows how processes are joined. The next step can only be possible, when both processes are completed.|
|![Data](./x_gitressourcen/Data.png)|Data elements|Indicates data that is being processed in the workflow. The rectangle can be attached to an activity. (See complex ADs &#8594; A3)|

---
There are further elements in an AD. For example, you can also show messages (sending signal and receiving). You can also add a time trigger (activity is triggered at a certain time). You can also add the data flow into the control flow (See [N3-More_AD](../N3-More_AD) &#8594; A3)

Here are more details, also explaining nested activities:

<https://sourcemaking.com/uml/modeling-business-systems/external-view/activity-diagrams>

<https://de.wikipedia.org/wiki/Aktivit%C3%A4tsdiagramm>

### Cheat Sheet AD:
[UML V2.5 - AD Overview](./Notationsuebersicht UML 2.5 3.pdf)


---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## Choose a UML tool:

Here are some tools you can use to draw activity diagrams:<br>
*Important: Symbols must be correct! Some of the tools do not represent the symbols correctly!*
		
**DrawIO** (recommended tool >> UML 2.5 open): <https://www.draw.io/>
*Note: use the template "AD Symbols.drawio"*

**StarUML** (Free ProfiTool WIN/mac): <http://staruml.io/>

**Modelio** (Free Profitool WIN) : <https://www.modelio.org/>

**UMLet** (UMLetino online): <https://www.umlet.com/>

**Violet** (Creates Html-Diagramme, needs Java 8): <http://alexdp.free.fr/violetumleditor/page.php>

**Visual Paradigm Online Editor** (Free Profitool): <https://online.visual-paradigm.com/de/diagrams/solutions/free-activity-diagram-editor-online/>

Or you can use **MS Visio** ([>>> TBZ EDU](https://portal.azure.com/#blade/Microsoft_Azure_Education/EducationMenuBlade/software)) or other graphical programs.

---

## Introduction AD with DrawIO

![Video:](../x_gitressourcen/Video.png)
[![Einführung](./x_gitressourcen/DrawIO.png)](https://web.microsoftstream.com/video/e3caa725-e52e-4702-b374-92b93af82778?list=studio)

[Tutorials Drawio](https://drawio-app.com/tutorials/)
---

## Introduction AD with StarUML

![Video:](../x_gitressourcen/Video.png)
[![Einführung StarUML](./x_gitressourcen/StarUML.png)](https://web.microsoftstream.com/video/61f4cc4b-f420-4d6b-b072-88bfab229a43)

[Doku StarUML AD](http://staruml.sourceforge.net/docs/user-guide(en)/ch05_6.html)

---

# Checkpoint
* I know the symbols of AD (Standard UML 2.5+!).
* I can read and understand a simple diagran.
* I have installed a tool and can draw diagrams.
